import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import FileIO 1.0


MenuBar {
    id : menubar

            Menu {
                title: qsTr("&File")
                MenuItem {
                    text: qsTr("&New")
                    shortcut: StandardKey.New
                    //onTriggered: editor.addTab("untitled", Qt.createComponent("TextEdit.qml"))
                    onTriggered: {
                        editor.insertTab(indextab++,"untitled", Qt.createComponent("TextEdit.qml"))
                        console.log("New::indextab:"+indextab)
                    }
                }
                MenuItem {
                    text: qsTr("&Open")
                    shortcut: StandardKey.Open
                    onTriggered: {
                        console.log("Open::indextab:"+editor.currentIndex)
                        console.log(fileIO.readFile())
                    }
                }
                MenuItem {
                    text: qsTr("Reload from disc...")
                    shortcut: StandardKey.Refresh
                }
                MenuItem {
                    text: qsTr("Save")
                    shortcut: StandardKey.Save
                    onTriggered: {

                        console.log("Save::indextab:"+editor.currentIndex)

                        fileIO.writeFile(editor.currentIndex)

                        }
                }
                MenuItem {
                    text: qsTr("Save As...")
                    shortcut: StandardKey.SaveAs
                }
                MenuItem { text: qsTr("Save Copy as...") }
                MenuItem { text: qsTr("Save All") }
                MenuItem { text: qsTr("Rename") }
                MenuItem {
                    text: qsTr("Close")
                    shortcut: StandardKey.Close
                }
                MenuItem { text: qsTr("Save all but active document" )}
                MenuItem { text: qsTr("Close All" )}
                MenuSeparator {}
                MenuItem {
                    text: qsTr("&Print")
                    shortcut: StandardKey.Print
                }
                MenuItem { text: qsTr("Print Settings" )}
                MenuSeparator {}
                MenuItem {
                    text: qsTr("E&xit")
                    shortcut: StandardKey.Quit
                    onTriggered: Qt.quit()
                }
            }

            Menu {
                title: qsTr("&Edit")
                MenuItem {
                    text: qsTr("Undo")
                    shortcut: StandardKey.Undo
                }
                MenuItem {
                    text: qsTr("Redo")
                    shortcut: StandardKey.Redo
                }
                MenuSeparator {}
                MenuItem {
                    text: qsTr("Cut")
                    shortcut: StandardKey.Cut
                }
                MenuItem {
                    text: qsTr("Copy")
                    shortcut: StandardKey.Copy
                }
                MenuItem {
                    text: qsTr("Paste")
                    shortcut: StandardKey.Paste
                }
                MenuItem {
                    text: qsTr("Delete")
                    shortcut: StandardKey.Delete
                }
                MenuItem {
                    text: qsTr("Select All")
                    shortcut: StandardKey.SelectAll
                }
                MenuSeparator {}
                Menu {
                    title: qsTr("Copy to Clipboard")

                    MenuItem {text:qsTr("Current Fule File path to Clipboard")}
                    MenuItem {text:qsTr("Current Filename path to Clipboard")}
                }
                Menu {
                    title: qsTr("Convert Case to")

                    MenuItem {text:qsTr("UPPERCASE")}
                    MenuItem {text:qsTr("lowercase")}
                }
                Menu {
                    title: qsTr("Line Operations")

                    MenuItem {text:qsTr("Duplicate Current Line")}
                    MenuItem {text:"Split Lines"}
                    MenuItem {text:"Join Lines"}
                    MenuItem {text:"Sort Lines in Ascending Order"}
                    MenuItem {text:"Sort Lines in Descending Order"}
                    MenuItem {text:"Move Up Current Line"}
                    MenuItem {text:"Move Down Current Line"}
                    MenuItem {text:"Remove Empty lines"}
                    MenuItem {text:"Remove Empty lines (including white characters)"}
                    MenuItem {text:"Insert Blank Line Above Current"}
                    MenuItem {text:"Insert Blank Line Below Current"}
                }
                Menu {
                    title: "EOL Conversion"

                    MenuItem {text:"Windows Format"}
                    MenuItem {text:"Unix/Linux Format"}
                    MenuItem {text:"Old Mac Format"}
                }
                Menu {
                    title: "Blank Operations"

                    MenuItem {text:"Trim Leading Spaces"}
                    MenuItem {text:"Trim Trailing Spaces"}
                    MenuItem {text:"Trim Leading and Trailing Spaces"}
                    MenuItem {text:"EOL to Space"}
                    MenuItem {text:"Remove Unneccessary Blank and EOL"}
                    MenuSeparator {}
                    MenuItem {text:"TAB to spaces"}
                    MenuItem {text:"Spaces to TAB (All)"}
                    MenuItem {text:"Spaces to TAB (Leading)"}
                }
                MenuSeparator {}
                MenuItem { text: "Set Read-Only" }
            }

            Menu    {
                title: "&Settings"

                MenuItem{text:"Preferences..."}
                MenuItem{text:"Style Configurator..."}
                MenuItem{text:"Shortcut Mapper..."}
     }

            Menu    {
                title: "&Help"

                MenuItem{text:"Table Of Contents"}
                MenuSeparator {}
                MenuItem{text:"Editor Home Page"}
                MenuItem{text:"Forum"}
                MenuSeparator {}
                MenuItem{text:"About"}
            }
    }
