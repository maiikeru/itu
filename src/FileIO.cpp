#include "FileIO.h"

FileIO::FileIO(QObject *parent) :
    QObject(parent)
{
}

void FileIO::writeFile(const QString &inputText)
{
    QString fileName = tr("text.txt");
    QFile file(fileName);
    if(file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "created file:" << fileName;
        QTextStream stream (&file);
        stream << inputText <<endl;
        file.close();
        return;
    }
    else
    {
        qDebug() << "could not create file:" << fileName;
    }

}

QString FileIO::readFile() const
{

    QString fileName = tr("text.txt");
    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "open file:" << fileName;
        QString content = file.readAll();
        file.close();
        return content;
    }
    return "";
}

