#ifndef FILEIO_H
#define FILEIO_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QDebug>

class FileIO : public QObject
{
    Q_OBJECT
public:
    explicit FileIO(QObject *parent = 0);

    Q_INVOKABLE void writeFile(const QString &inputText);

    Q_INVOKABLE QString readFile() const;


signals:

public slots:

};

#endif // FILEIO_H
