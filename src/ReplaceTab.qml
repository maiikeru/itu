import QtQuick 2.0
import QtQuick.Controls 1.2

Tab {
    title:"Nahradit"
    anchors.leftMargin:5
    anchors.topMargin:30

    Column  {
        spacing:10
        //anchors.leftMargin:10

        Text    {
            text: "Najit"
        }

        Rectangle   {
             color:"white"
             border.color:"gray"
             anchors.horizontalCenter:parent.horizontalCenter
             width:parent.width - 15
             height:20

            TextInput {
               text:""
               height:20
            }
        }

        Text    {
            text: "Nahradit"
        }

        Rectangle   {
             color:"white"
             border.color:"gray"
             anchors.horizontalCenter:parent.horizontalCenter
             width:parent.width - 15
             height:20

            TextInput {
               text:""
               height:20
            }
        }


        CheckBox {
            text: qsTr("Case-Insensitive")
        }
        CheckBox {
            text: qsTr("Match whole word only")
            checked:true
        }

        GroupBox{
            title:"Search Mode"
            width:parent.width - 15

            ExclusiveGroup {
                id: search_mode
            }

            Column {
                spacing:10

                RadioButton {
                    text: qsTr("Normal")
                    exclusiveGroup: search_mode
                    checked:true
                }
                RadioButton {
                    text: qsTr("Regular Expression")
                    exclusiveGroup: search_mode
                }
                RadioButton {
                    text: qsTr("Extended (\\n, \\r, \\t, \\0, \\x...)")
                    exclusiveGroup: search_mode
                }

            }
        }
    }

}
