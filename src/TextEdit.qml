import QtQuick 2.3
import QtQuick.Controls.Styles 1.2
import QtQuick.Controls 1.2
Rectangle{

    id:textEditor

    function paste() { textArea.paste() }
    function copy() { textArea.copy() }
    function selectAll() { textArea.selectAll() }

    property alias textContent: textArea.text
    Flickable {
        id: flickArea
        width: parent.width; height: parent.height
        anchors.fill:parent

        boundsBehavior: Flickable.StopAtBounds
        flickableDirection: Flickable.VerticalFlick
        interactive: true
        clip:true



        TextArea {
            id: textArea
            anchors.fill:parent
            width:parent.width; height:parent.height
            focus: true
            font.pointSize:10
            selectByMouse: true

        }



        
    }

}
