#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "FileIO.h"
#include <QtQml>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<FileIO>( "FileIO", 1, 0, "FileIO" );

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
