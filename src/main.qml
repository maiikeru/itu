import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Window 2.1
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.0
import FileIO 1.0
import "./"
//import "./AppMenu.qml"


ApplicationWindow {
    title: qsTr("Editor")
    visible: true
    width:850
    height:800
    //width: Screen.desktopAvailableWidth
    //height: Screen.desktopAvailableHeight
    color: "#414141"

    property int indextab: 0

    menuBar : AppMenu { }

    toolBar : Toolbar { }


    SplitView   {
        anchors.fill: parent
        orientation: Qt.Horizontal

        Rectangle   {
            width: 250
            //Layout.maximumWidth: 100




            TabView {
                anchors.fill: parent

                SearchTab { }


                ReplaceTab { }



                style: TabViewStyle {
                        frameOverlap: 1
                        tab: Rectangle {
                            color: "gray"
                            //color: styleData.selected ? "steelblue" :"lightsteelblue"
                            //border.color:"black"
                            implicitWidth: Math.max(text.width + 4, 80)
                            implicitHeight: 20
                            radius: 2
                            Text {
                                id: text
                                anchors.centerIn: parent
                                text: styleData.title
                                //color: "black"
                                color: styleData.selected ? "white" : "black"
                            }
                        }
                        frame: Rectangle { color: "gray" }
                    }

            }

        }

        Rectangle   {
            width:400

            SplitView   {
                anchors.fill: parent
                orientation:  Qt.Vertical

                /*Rectangle   {
                    height: 500
                    //Layout.minimumWidth: 100


                    TextArea    {
                        text: "Text Editor"
                        anchors.fill:parent
                    }

                }*/
                TabView {
                    height:450
                    id:editor

                    Tab{
                        title:"main.cpp"
                        id:tab1


                        TextEdit{textContent: "#include <iostream>\n\nint main() {\n\treturn 0;\n}"}

                    }

                    Tab{
                        title:"New Document"

                        TextArea    {
                            width: 200
                            height: 100
                            anchors.fill:parent
                        }


                    }

                }


               TabView {
                   Tab    {
                       title: "Console"
                       anchors.fill: parent

                       Text {
                           text: "Virtualni konzole"
                       }

                   }

                   Tab    {
                       title: "Log"
                       anchors.fill: parent

                       Text {
                           text: "8:00:00 Application starter\n8:00:20 File /etc/apache2/log.log opened\n8:02:00 File /etc/apache2/sites-enables/site.conf opened\n8:02:54 File /etc/apache2/sites-enables/site.conf opened"
                       }


                   }

                }



/*
                Rectangle   {
                    height: 150
                    //Layout.minimumWidth: 100

                    Text    {
                        text: "Console"
                    }

                }
*/
            }



        }

    }

    FileIO{
        id:fileIO


    }

}
